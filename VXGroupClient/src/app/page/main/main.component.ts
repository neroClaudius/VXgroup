import { FilterData } from './../../filter/filter.component';
import { Page } from './../../model/page';
import { NewsService } from './../../services/news.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { News } from '../../model/news';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

  page: Page<News>;
  currentPage: number;
  constructor(private newService: NewsService) { }

  ngOnInit() {
    this.currentPage = 0;
    this.loadData();
  }

  loadData(filterData?: FilterData) {
    if (filterData === undefined) {
      this.newService.getNews(this.currentPage).subscribe(
        (res) => {
          this.page = res;
        });
    } else {
      this.newService.getNews(this.currentPage, filterData.dataName, filterData.categoryId).subscribe(
        (res) => {
          this.page = res;
        });
    }
  }
  changePage(i: number) {
    this.currentPage += i;
    this.loadData();

  }

  details() {

  }
}

import { News } from './../model/news';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NewsService } from '../services/news.service';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewsDetailsComponent implements OnInit {
  id: number;
  news: News;
  constructor(private route: ActivatedRoute, private newsServ: NewsService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.newsServ.getSingleNews(this.id).subscribe(
        (res) => {
          this.news = res;
        })
    })
  }



}

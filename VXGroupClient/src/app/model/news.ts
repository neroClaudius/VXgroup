import { Category } from './category';
export interface News {
    id?: number;
    title: string;
    desription: string;
    content: string;
    category: Category;
}
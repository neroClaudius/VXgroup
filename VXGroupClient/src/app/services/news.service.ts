import { News } from './../model/news';
import { Page } from './../model/page';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class NewsService {

  constructor(private http: HttpClient) { }

  getNews(page: number, title?: string, id?: number): Observable<Page<News>> {

    let params = new HttpParams();
    params = params.append('page', page.toString());
    params = params.append('size', '5');
    if (title) {
      params = params.append('title', title);
    }
    if (+id) {
      params = params.append('id', id.toString());
    }
    return this.http.get<Page<News>>('api/news', { params });
  }

  getSingleNews(id: number): Observable<News> {
    return this.http.get<News>(`api/news/${id}`);
  }
}

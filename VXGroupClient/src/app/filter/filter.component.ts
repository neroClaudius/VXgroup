import { CategoryService } from './../services/category.service';
import { Category } from './../model/category';
import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FilterComponent implements OnInit {
  filterData = {
    dataName : '',
    categoryId : 0
  }
  @Output() filterEmiter : EventEmitter<FilterData> = new EventEmitter();
  categories: Category[];
  constructor(private categService: CategoryService) { }

  ngOnInit() {
    this.loadCategs();
  }
  loadCategs() {
    this.categService.getCategories().subscribe(
      (res) => {
        this.categories = res;
      });
  }
  filter(){
    this.filterEmiter.emit(this.filterData);
  }

}
export interface FilterData{
  dataName : string;
  categoryId : number;
}

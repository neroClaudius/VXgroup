package vp.advancedjava.students.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.model.News;
import vp.advancedjava.students.service.NewsService;

@RestController
public class NewsController {

	@Autowired
	private NewsService newsService;
	
    @GetMapping(value = "api/news")
    public ResponseEntity<Page<News>> getAll(Pageable pageable,@RequestParam(required = false) 
    String title,@RequestParam(required = false) Long id){
    	 Page<News> retVal ;
    	
    	if(title != null || id != null) {
    		retVal = newsService.findByTitleAndId(title, id, pageable);
   		
    	}else {
    		retVal = newsService.findAll(pageable);
    	}
    	return  new ResponseEntity<>(retVal,HttpStatus.OK);
    	
    }
    
    @GetMapping(value = "api/news/{id}")
    public ResponseEntity<News> getNews(@PathVariable Long id) {
        News news = newsService.findOne(id);

        if (news == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(news, HttpStatus.OK);
    }
    
    @PostMapping(value = "api/news")
    public ResponseEntity<News> create(@RequestBody News news) {
        final News savedNews = newsService.save(news);
        return new ResponseEntity<>(savedNews, HttpStatus.CREATED);
    }

    @PutMapping(value = "api/news/{id}")
    public ResponseEntity<News> update(	@PathVariable Long id,
    									@RequestBody News news) {
        News foundNews = newsService.findOne(id);
        if (news == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        foundNews.setContent(news.getContent());
        foundNews.setDescription(news.getDescription());
        foundNews.setTitle(news.getTitle());
        foundNews.setCategory(news.getCategory());
        News savedNews = newsService.save(foundNews);
        return new ResponseEntity<>(savedNews, HttpStatus.CREATED);
    }
    
    @SuppressWarnings("rawtypes")
	@DeleteMapping(value = "api/news/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        final News news = newsService.findOne(id);
        if (news == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        newsService.remove(id);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    	

}

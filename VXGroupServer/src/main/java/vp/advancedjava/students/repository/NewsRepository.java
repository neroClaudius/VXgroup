package vp.advancedjava.students.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.News;

@Component
public interface NewsRepository extends JpaRepository<News, Long> {
	
	public Page<News> findByTitleContainsAndCategoryId(String title,Long catId,Pageable page);

}

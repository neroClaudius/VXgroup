package vp.advancedjava.students.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.News;
import vp.advancedjava.students.repository.NewsRepository;

@Component
public class NewsService {

	@Autowired
	NewsRepository newsRepository;
	
	public List<News> findAll() {
		return newsRepository.findAll();
	}
	
	public Page<News> findAll(Pageable page) {
		return newsRepository.findAll(page);
	}

	public News findOne(Long id) {
		return newsRepository.findOne(id);
	}

	public News save(News news) {
		return newsRepository.save(news);
	}

	public void remove(Long id) {
		newsRepository.delete(id);
	}
	
	public Page<News> findByTitleAndId(String title,Long catId,Pageable page){
		return newsRepository.findByTitleContainsAndCategoryId(title, catId,page);
	}

}

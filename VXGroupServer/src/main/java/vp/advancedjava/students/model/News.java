package vp.advancedjava.students.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class News {

	@Id
	@GeneratedValue
	private Long id;

	private String title;
	
	private String description;
	
	private String content;
	
	@ManyToOne(fetch = FetchType.EAGER) 
	private Category category;

	public News() {
		super();
	}

	public News(Long id, String title, String description, String content, Category category) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.content = content;
		this.category = category;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}
